package fr.obeat.controllers

import fr.obeat.entities.MusicEntity
import fr.obeat.services.GeneratorService
import fr.obeat.viewModels.generator.GenerateTheme
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/generator")
class GeneratorController {

    @Autowired
    val generatorService: GeneratorService? = null

    @PostMapping("/add")
    fun generateTheme(vm: GenerateTheme): MusicEntity {
        return generatorService!!.generateMusic(vm)
    }

}
