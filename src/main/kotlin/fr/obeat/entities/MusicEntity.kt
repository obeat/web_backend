package fr.obeat.entities

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "musics")
data class MusicEntity (
    @Id
    @GeneratedValue
    val id: Int = 0,

    val time: Float,
    val bpm: Int,
    val theme: Byte
)