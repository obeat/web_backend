package fr.obeat.repositories

import fr.obeat.entities.MusicEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface MusicRepository : JpaRepository<MusicEntity, Int>
