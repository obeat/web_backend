package fr.obeat.services

import fr.obeat.entities.MusicEntity
import fr.obeat.repositories.MusicRepository
import fr.obeat.viewModels.generator.GenerateTheme
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.Exception

@Service
class GeneratorService{
    @Autowired
    private val musicRepositoy: MusicRepository? = null

    fun generateMusic(vm: GenerateTheme) : MusicEntity
    {
        if (vm.time < 0.01)
            throw Exception("Timer inferior to 0.01m")
        val music = MusicEntity(0, vm.time, vm.bpm, vm.theme)
        musicRepositoy!!.save(music)
        return music
    }
}
