package fr.obeat.viewModels.generator

data class GenerateTheme(
    val time: Float,
    val bpm: Int,
    val theme: Byte
)
